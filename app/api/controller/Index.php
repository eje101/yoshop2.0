<?php

namespace app\api\controller;

use think\facade\Db;

/**
 * 默认控制器
 * Class Index
 * @package app\api\controller
 */
class Index
{
    public function index()
    {
//        $list =Db::name('region')->select()->toArray();
        $list=[1,2,3,4];
        $data = array_map(function ($id) {
            return [
                'image_id' => $id,
                'store_id' => 2
            ];
        }, $list);
        return json($data);

        $res = $this->getTree($list);
        return json($res);
        echo '当前访问的index.php，请将index.html设为默认站点入口';
    }

    private function getTree($list,$pid=0){
        $tree = [];
        foreach ($list as $key=>$value){
            if($value['pid']==$pid){
                $children = $this->getTree($list,$value['id']);
                !empty($children) && $value['children'] = $children;
                $tree[] = $value;
                unset($list[$key]);
            }

        }
        return $tree;
    }

    private function getTreeList($allList)
    {
        $treeList = [];
        foreach ($allList as $pKey => $province) {
            if ($province['level'] == 1) {    // 省份
                $treeList[$province['id']] = $province;
                unset($allList[$pKey]);
                foreach ($allList as $cKey => $city) {
                    if ($city['level'] == 2 && $city['pid'] == $province['id']) {    // 城市
                        $treeList[$province['id']]['city'][$city['id']] = $city;
                        unset($allList[$cKey]);
                        foreach ($allList as $rKey => $region) {
                            if ($region['level'] == 3 && $region['pid'] == $city['id']) {    // 地区
                                $treeList[$province['id']]['city'][$city['id']]['region'][$region['id']] = $region;
                                unset($allList[$rKey]);
                            }
                        }
                    }
                }
            }
        }
        return $treeList;
    }
}